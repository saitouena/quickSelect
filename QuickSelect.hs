import qualified Data.List as DL
import qualified Data.List.Split as DLS
--import qualified System.Random as SR

quickSelect :: [Int] -> Int -> Int
quickSelect [a] 0 = a
quickSelect as k
  | length as <= k = error "k >= length as"
  | otherwise = let x = getPivot as
                    smaller = filter (< x) as
                    equals = filter (== x) as
                    bigger = filter (> x) as
                    sl = length smaller
                    el = length equals
                in
                  case k of
                    _ | sl<=k&&k<sl+el -> x
                      | k < sl -> quickSelect smaller k
                      | otherwise -> quickSelect bigger (k-sl-el)

getPivot as = let len = length as
                  xs = map medianForGetPivot (DLS.chunksOf 5 as)
              in quickSelect xs (len`div`10)

split as k = if length as <= k
             then [median as]
             else (median (take k as)):(split (drop k as) k)

median [] = error "meadian []"
median as = let k = (length as)`div`2
            in DL.sort as !! k

medianForGetPivot as = let k = (length as)`div`2
                       in DL.sort as !! k

-- https://rosettacode.org/wiki/Sorting_algorithms/Insertion_sort
insertionSort :: Ord a => [a] -> [a]
insertionSort = foldr DL.insert []

fastMedian [] = error "fastMedian []"
fastMedian as = let k = (length as)`div`2
                in quickSelect as k

main = do
--  g <- SR.getStdGen
  let seq = [1..(10^7)]--take 100000 (SR.randoms g) :: [Int]
--  print seq
  putStrLn "O(nlogn)"
  print $ median seq
  putStrLn "O(n)"
  print $ fastMedian seq
